<?php
namespace Helpers;

class Database
{
  // Be sure to include a database config to define the Database secrets
  private $host, $user, $pass, $dbname;

  // Database connection
  private $dbc;

  // Error variable
  private $error;

  // Statement
  private $stmt;

  /**
   * Constructor
   *
   * Create connection string
   * Create PDO object
   */
  public function __construct($host = DB_HOST, $user = DB_USER, $pass = DB_PASS, $dbname = DB_NAME, $adapter)
  {
    $this->host = $host;
    $this->user = $user;
    $this->pass = $pass;
    $this->dbname = $dbname;
    // Set connection string
    switch ($adapter) {
      case 'mysql':
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
        break;
      case 'sqlsrv':
        $dsn  = 'sqlsrv:Server=' . $this->host .';Database=' . $this->dbname .';';
        break;
      default:
        $this->error = 'This database driver is not supported, please choose either mysql or sqlsrv';
    }


    // Set PDO Options
    $options = array(
      \PDO::ATTR_PERSISTENT => true,
      \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
    );

    // Create PDO instance and catch error
    try {
      $this->dbc = new \PDO($dsn, $this->user, $this->pass, $options);
    } catch (PDOException $e) {
      $this->error = $e->getMessage();
    }
  }

  /**
   * Prepare PDO query
   * @param string $query
   */
  public function query($query)
  {
    $this->stmt = $this->dbc->prepare($query);
  }

  /**
   * Bind param to prepared PDO query
   * @param string $param
   * @param string $value
   * @param string|null $type
   */
  public function bind($param, $value, $type = null)
  {
    // If no type given
    if (is_null($type)) {
      switch (true) {
        // Set type to int
        case is_int($value):
          $type = \PDO::PARAM_INT;
          break;

        // Set type to bool
        case is_bool($value):
          $type = \PDO::PARAM_BOOL;
          break;

        // Set type to null
        case is_null($value):
          $type = \PDO::PARAM_NULL;
          break;

        // Default type to string
        default:
          $type = \PDO::PARAM_STR;
      }
    }

    // Bind parameter to prepared query
    $this->stmt->bindValue($param, $value, $type);
  }

  /**
   * Execute prepared query
   * @return PDO::object Object for executed query
   */
  public function execute()
  {
    return $this->stmt->execute();
  }

  /**
   * Get all the results from executed query
   * @return array All the results from executed query
   */
  public function resultSet()
  {
    $this->execute();
    return $this->stmt->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Get the first result of the executed query
   * @return array The Last row of the executed query
   */
  public function single()
  {
    $this->execute();
    return $this->stmt->fetch(\PDO::FETCH_ASSOC);
  }

  /**
   * Get the count of returned rows of the executed query
   * @return int Count of the rows
   */
  public function rowCount()
  {
    return $this->stmt->rowCount();
  }

  /**
   * Returns the ID of the last inserted row, or the last value from a sequence object
   * @return string
   */
  public function lastInsertId()
  {
    return $this->stmt->lastInsertId();
  }

  /**
   * Start a transaction
   * @return bool
   */
  public function beginTransaction()
  {
    return $this->dbc->beginTransaction();
  }

  /**
   * Ends the transaction
   * @return bool
   */
  public function endTransaction()
  {
    return $this->dbc->commit();
  }

  /**
   * Cancels an already started transaction, also rollsback the changes
   * @return bool
   */
  public function cancelTransaction()
  {
    return $this->dbc->rollBack();
  }

  /**
   * Gives all the details regarding a prepared statement
   * @return void dumps to stdout
   */
  public function debugDumpParams()
  {
    return $this->stmt->debugDumpParams();
  }
}
